-- adds all static parts

-------------
local PART={}
PART.ID = "eorexterior_interiorbox"
PART.Model = "models/FuzzyLeo/EdgeOfReality/ExteriorJodie.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	hook.Add("SkinChanged", "eorexterior-interiorbox", function(ent,i)
		if ent.TardisExterior then
			local eorjodie_interiorpb=ent:GetPart("eorexterior_interiorbox")
			if IsValid(eorexterior_interiorboxb) then
				eorexterior_interiorbox:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local eorexterior_interiorbox=ent.interior:GetPart("eorexterior_interiorbox")
				if IsValid(eorexterior_interiorbox) then
					eorexterior_interiorbox:SetSkin(i)
				end
			end
		end
	end)
end

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorexterior_interiorbox2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/interiorbox.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	hook.Add("SkinChanged", "eorexterior-interiorbox2", function(ent,i)
		if ent.TardisExterior then
			local eorjodie_interiorpb2=ent:GetPart("eorexterior_interiorbox2")
			if IsValid(eorexterior_interiorboxb2) then
				eorexterior_interiorbox2:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local eorexterior_interiorbox2=ent.interior:GetPart("eorexterior_interiorbox2")
				if IsValid(eorexterior_interiorbox2) then
					eorexterior_interiorbox2:SetSkin(i)
				end
			end
		end
	end)
end

TARDIS:AddPart(PART)