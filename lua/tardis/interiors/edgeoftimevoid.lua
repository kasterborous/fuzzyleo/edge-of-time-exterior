-- Hell Bent TARDIS

local T={}
T.Base="edgeoftimepolicebox"
T.Name="EOT Void"
T.ID="edgeoftimevoid"

T.IsVersionOf = "eedgeoftimepolicebox"

T.Interior={
        IdleSound = {
                {
                    path = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Atmos_Loop.wav",
                    volume = 1
                },
            },
	Parts={
        eorexterior_interiorbox2            =   {},
        eorexterior_interiorbox            =   false,
	},
}


TARDIS:AddInterior(T)